package com.formation.epsi.java.superherogestion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperherogestionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuperherogestionApplication.class, args);
	}
}



//package com.formation.epsi.java.superherogestion;
//
//import com.formation.epsi.java.superherogestion.model.Power;
//import com.formation.epsi.java.superherogestion.model.SuperHero;
//import com.formation.epsi.java.superherogestion.repository.SuperHeroRepository;
//import com.formation.epsi.java.superherogestion.repository.PowerRepository;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//
//import javax.annotation.PostConstruct;
//import java.util.ArrayList;
//import java.util.List;
//
//@SpringBootApplication
//public class SuperherogestionApplication {
//
//	Logger logger = LoggerFactory.getLogger(SuperherogestionApplication.class);
//
//	private final PowerRepository powerRepository;
//	private final SuperHeroRepository superHeroRepository;
//
//	SuperherogestionApplication(
//			PowerRepository powerRepository,
//			SuperHeroRepository superHeroRepository
//	){
//		this.powerRepository = powerRepository;
//		this.superHeroRepository = superHeroRepository;
//	}
//
//	public static void main(String[] args) {
//		SpringApplication.run(SuperherogestionApplication.class, args);
//	}
//
//	@PostConstruct
//	public void initData() {
//		if (powerRepository.count() == 0) {
//			logger.info("New Power created : {}", powerRepository.save(createPower("Vol", "Permet de voler")));
//		}
//
//		if (superHeroRepository.count() == 0) {
//			SuperHero superHero = new SuperHero();
//			superHero.setSuperHeroName("Superman");
//			superHero.setSecretIdentity("Clark Kent");
//
//			List<Power> powers = new ArrayList<>();
//			powerRepository.findById(1L).ifPresent(powers::add);
//
//			powers.add(createPower("Super Force", "Permet de taper fort"));
//
//			SuperHero createdSuperHero = superHeroRepository.save(superHero);
//			createdSuperHero.setPowers(powers);
//			createdSuperHero = superHeroRepository.save(superHero);
//			logger.info("New SuperHero created : {}", createdSuperHero);
//		}
//	}
//
//	private Power createPower(String name, String description) {
//		Power power = new Power();
//		power.setName(name);
//		power.setDescription(description);
//		return power;
//	}
//
//}



