package com.formation.epsi.java.superherogestion.api.DTO;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import lombok.*;

@Data
@Builder
public class PowerDTO {
    private long id;
    private String name;
    private String description;
}

