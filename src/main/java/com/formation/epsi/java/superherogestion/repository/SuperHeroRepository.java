package com.formation.epsi.java.superherogestion.repository;

import com.formation.epsi.java.superherogestion.model.SuperHero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuperHeroRepository extends JpaRepository<SuperHero,Long>{

    //Optional<SuperHero> getfirstBySuperHeroName


}
