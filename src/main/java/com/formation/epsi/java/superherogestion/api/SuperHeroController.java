package com.formation.epsi.java.superherogestion.api;

import com.formation.epsi.java.superherogestion.api.DTO.SuperHeroDTO;
import com.formation.epsi.java.superherogestion.model.SuperHero;
import com.formation.epsi.java.superherogestion.repository.SuperHeroRepository;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping(
        path = "/superHeroes",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class SuperHeroController {

    private final SuperHeroRepository superHeroRepository;

    SuperHeroController(
            SuperHeroRepository superHeroRepository
    ) {
        this.superHeroRepository = superHeroRepository;
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<SuperHeroDTO> getById(@PathVariable Long id) {
        return this.superHeroRepository.findById(id)
                .map(superHero -> ResponseEntity.ok(mapToDTO(superHero)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<List<SuperHeroDTO>> getAll() {
//         List<SuperHero> superHeroes = this.superHeroRepository.findAll();
//         List<SuperHeroDTO> superHeroDTOS = new ArrayList<>();
//         superHeroes.forEach(superHero -> superHeroDTOS.add(mapToDTO(superHero)));
//
//        return ResponseEntity.ok(superHeroDTOS);
        return ResponseEntity.ok(
                this.superHeroRepository
                        .findAll()
                        .stream()
                        .map(this::mapToDTO)
                        .collect(Collectors.toList())
        );
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuperHeroDTO> create(@RequestBody SuperHeroDTO superHeroDTO) {
        superHeroDTO.setId(0);
        SuperHero superHero = mapToEntity(superHeroDTO);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapToDTO(this.superHeroRepository.save(superHero)));
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<SuperHeroDTO> update(
            @PathVariable Long id,
            @RequestBody SuperHeroDTO superHeroDTO)
    {
        Optional<SuperHero> superHeroOptional = this.superHeroRepository.findById(id);
        if (superHeroOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        SuperHero superHeroToUpdate = mapToEntity(superHeroDTO);
        return ResponseEntity.ok(mapToDTO(this.superHeroRepository.save(superHeroToUpdate)));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.superHeroRepository.deleteById(id);
    }












    private SuperHero mapToEntity(SuperHeroDTO superHeroDTO) {
        SuperHero superHero = new SuperHero();
        superHero.setId(superHeroDTO.getId());
        superHero.setSuperHeroName(superHeroDTO.getSuperHeroName());
        superHero.setSecretIdentity(superHeroDTO.getSecretIdentity());
        return superHero;
    }

    private SuperHeroDTO mapToDTO(SuperHero superHero) {
        return new SuperHeroDTO(
                superHero.getId(),
                superHero.getSuperHeroName(),
                superHero.getSecretIdentity()
        );
    }
}

